package tdt.basicjapanese.vocabulary;

import java.util.ArrayList;
import tdt.basicjapanese.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class VocabularyListAdapter extends ArrayAdapter<VocabularyItem> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<VocabularyItem> mListData = new ArrayList<VocabularyItem>();

    public VocabularyListAdapter(Context mContext, int layoutResourceId, ArrayList<VocabularyItem> mListData) {
        super(mContext, layoutResourceId, mListData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mListData = mListData;
    }

    public void setGridData(ArrayList<VocabularyItem> mListData) {
        this.mListData = mListData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.titleTextView = (TextView) row.findViewById(R.id.tv_titleVocabulary);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        VocabularyItem item = mListData.get(position);
        holder.titleTextView.setText(item.getTitle());
        return row;
    }

    static class ViewHolder {
        TextView titleTextView;
    }
}