package tdt.basicjapanese.vocabulary;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import tdt.basicjapanese.R;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class VocabularyActivity extends ActionBarActivity {

	private int pos;
	private String mTitle, mFile;
	private ListView list;
    private TextToSpeech myTTS;
    private int MY_DATA_CHECK_CODE = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.item_vocabulary);
		final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        list = (ListView) findViewById(R.id.lst_ItemVocabulary);
        Bundle extras = getIntent().getExtras();
        pos = extras.getInt("position");
        mTitle = extras.getString("title");
        mFile = extras.getString("file");
        actionBar.setTitle(mTitle);
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);
        parseResult(loadJSONFromAsset(pos)); 
	}
	
	@Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	public String loadJSONFromAsset(int position) {
        String json = null;
        try {
        	String url = "json/vocabulary/"+mFile+".json";
            InputStream is = getAssets().open(url);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                //the user has the necessary data - create the TTS
            	 myTTS = new TextToSpeech(getApplicationContext(), new OnInitListener() {
					@Override
					public void onInit(int status) {
						// TODO Auto-generated method stub
						if (status == TextToSpeech.SUCCESS) {
				        	Locale locale = new Locale("jp_JP");
                            if(myTTS.isLanguageAvailable(locale)==TextToSpeech.LANG_AVAILABLE)
				                myTTS.setLanguage(locale);
				        }
				        else if (status == TextToSpeech.ERROR) {
				            Toast.makeText(getApplicationContext(), "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
				        }
					}
				}); 
            }
            else {
                    //no data - install it now
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
    }

    private void parseResult(String result) {
        try {
        	final ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
            SimpleAdapter mSchedule = null;
            HashMap<String, String> map;
            JSONObject response = new JSONObject(result);
            JSONArray vocabulary = response.optJSONArray(mFile);
            for (int i = 0; i < vocabulary.length(); i++) {
                JSONObject post = vocabulary.optJSONObject(i);
                String mKanji = post.optString("kanji");
                String mKana = post.optString("kana");
                String mRomaji = post.optString("romaji");
                String mVn = post.optString("vn");
                map = new HashMap<String, String>();
                map.put("kanji", mKanji);
                map.put("kana", mKana);
                map.put("romaji", mRomaji);
                map.put("vn", mVn);
                mylist.add(map);    
            }
            mSchedule = new SimpleAdapter(this, mylist, R.layout.list_item_vocabulary,
                    new String[] {"kanji", "kana", "romaji", "vn"}, new int[] {R.id.tv_VKanji, R.id.tv_VKana, R.id.tv_VRomaji, R.id.tv_VVn});
            list.setAdapter(mSchedule);
            list.setOnItemClickListener(new OnItemClickListener() {
            	
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					String speech = mylist.get(position).get("kana").toString();
					if (mylist.get(position).get("kana").isEmpty())
						speech = mylist.get(position).get("kanji").toString();
		            myTTS.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
				}
			});
	    } catch (JSONException e) {
	        e.printStackTrace();
	    }
    }
}