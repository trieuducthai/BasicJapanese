package tdt.basicjapanese.vocabulary;

public class VocabularyItem {
	private String title;
	private String file;
	
	public VocabularyItem() {
		
	}
	
	public VocabularyItem(String title, String file) {
		this.title = title;
		this.file = file;
	}
	
	public String getTitle() {
		return title;
	}
	
	public VocabularyItem setTitle(String title) {
		this.title = title;
		return this;			
	}
	
	public String getFile() {
		return file;
	}
	
	public VocabularyItem setFile(String file) {
		this.file = file;
		return this;				
	}
}
