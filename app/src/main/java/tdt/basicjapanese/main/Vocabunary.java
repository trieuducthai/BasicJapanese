package tdt.basicjapanese.main;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tdt.basicjapanese.R;
import tdt.basicjapanese.vocabulary.VocabularyActivity;
import tdt.basicjapanese.vocabulary.VocabularyItem;
import tdt.basicjapanese.vocabulary.VocabularyListAdapter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class Vocabunary extends ActionBarActivity {
    private ListView mListView;
    private VocabularyListAdapter mListAdapter;
    private ArrayList<VocabularyItem> mListData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vocabunary_layout);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mListView = (ListView) findViewById(R.id.lst_vocabulary);
        mListData = new ArrayList<>();
        mListAdapter = new VocabularyListAdapter(this, R.layout.grid_item_vocabulary, mListData);
        //Initialize with empty data     
        new AsyncTask<Void,Void,Void>() {
        	private ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
            	dialog = ProgressDialog.show(Vocabunary.this, "", "Loading...");
            }
            @Override
            protected Void doInBackground(Void... voids) {
            	parseResult(loadJSONFromAsset());
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                dialog.dismiss();
            }
        }.execute();
        mListAdapter.setGridData(mListData);
		mListView.setAdapter(mListAdapter);
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent myIntent = new Intent(Vocabunary.this, VocabularyActivity.class);
				Bundle extras = new Bundle();
				extras.putInt("position", position);
				extras.putString("title", mListData.get(position).getTitle());
				extras.putString("file", mListData.get(position).getFile());
				myIntent.putExtras(extras);
				startActivity(myIntent);
			}
		});
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("json/vocabulary/vocabulary.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray lesson = response.optJSONArray("vocabulary");
            VocabularyItem item;
            for (int i = 0; i < lesson.length(); i++) {
                JSONObject post = lesson.optJSONObject(i);
                String title = post.optString("title");
                String file = post.optString("file");
                item = new VocabularyItem();
                item.setTitle(title);
                item.setFile(file);
                mListData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}