package tdt.basicjapanese.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.Toast;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;
import tdt.basicjapanese.R;

public class MainActivity extends Activity {
	private ImageButton btnAlphabet;
	private ImageButton btnLesson;
	private ImageButton btnVolcabulary;
	private ImageButton btnAbout;
	private boolean doubleBackToExitPressedOnce = false;
	private StartAppAd startAppAd = new StartAppAd(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		StartAppSDK.init(this, "207919874", false);
		setContentView(R.layout.activity_main);
		btnAlphabet = (ImageButton) findViewById(R.id.btn_Alphabet);
		btnLesson = (ImageButton) findViewById(R.id.btn_Lesson);
		btnVolcabulary = (ImageButton) findViewById(R.id.btn_Vol);
		btnAbout = (ImageButton) findViewById(R.id.btn_About);
		buttonTouch();
		//Tạo Slider
		StartAppAd.showAd(this);
		//Load quảng cáo full từ startapp về, không có dòng này sẽ không có quảng cáo để hiện
		startAppAd.loadAd(StartAppAd.AdMode.AUTOMATIC);
		//Hiện quảng cáo full màn hình
		StartAppAd.showSplash(this, savedInstanceState);
	}

	private void buttonTouch() {
		btnAlphabet.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN ) {
					btnAlphabet.setBackgroundResource(R.drawable.alphabet_button_down);

		        }
		         else if (event.getAction() == MotionEvent.ACTION_UP ) {
		        	 btnAlphabet.setBackgroundResource(R.drawable.alphabet_button);
		        	 Intent myIntent = new Intent(MainActivity.this, Alphabet.class);
					 startActivity(myIntent);
		        }
				return false;
			}
		});
		
		btnLesson.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN ) {
					 btnLesson.setBackgroundResource(R.drawable.lesson_button_down);

		        }
		         else if (event.getAction() == MotionEvent.ACTION_UP ) {
		        	 btnLesson.setBackgroundResource(R.drawable.lesson_button);
		        	 Intent myIntent = new Intent(MainActivity.this, Lesson.class);
					 startActivity(myIntent);
		        }
				return false;
			}
		});

		btnVolcabulary.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN ) {
					 btnVolcabulary.setBackgroundResource(R.drawable.volcabulary_button_down);
		
		        }
		         else if (event.getAction() == MotionEvent.ACTION_UP ) {
		        	 btnVolcabulary.setBackgroundResource(R.drawable.volcabulary_button);
		        	 Intent myIntent = new Intent(MainActivity.this, Vocabunary.class);
					 startActivity(myIntent);
		        }
				return false;
			}
		});

		btnAbout.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN ) {
					btnAbout.setBackgroundResource(R.drawable.about_button_down);
		
		        }
		         else if (event.getAction() == MotionEvent.ACTION_UP ) {
		        	 btnAbout.setBackgroundResource(R.drawable.about_button);
		        	 Intent myIntent = new Intent(MainActivity.this, About.class);
					 startActivity(myIntent);
		        }
				return false;
			}
		});
		
	}
	
	 public void onBackPressed() {
	    if (doubleBackToExitPressedOnce) {
	        super.onBackPressed();
	        return;
	    }
	
	    this.doubleBackToExitPressedOnce = true;
	    Toast.makeText(this, "Nhấn lại để thoát!", Toast.LENGTH_SHORT).show();
	
	    new Handler().postDelayed(new Runnable() {
	
	        @Override
	        public void run() {
	            doubleBackToExitPressedOnce=false;
	        }
	    }, 2000);
	 }
}
