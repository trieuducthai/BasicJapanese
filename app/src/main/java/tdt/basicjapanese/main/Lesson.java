package tdt.basicjapanese.main;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tdt.basicjapanese.R;
import tdt.basicjapanese.lesson.LessonActivity;
import tdt.basicjapanese.lesson.LessonGridAdapter;
import tdt.basicjapanese.lesson.LessonItem;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

public class Lesson extends ActionBarActivity {
    private static final String TAG = Lesson.class.getSimpleName();
    private GridView mGridView;
    private LessonGridAdapter mGridAdapter;
    private ArrayList<LessonItem> mGridData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lesson_layout);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mGridView = (GridView) findViewById(R.id.grid_lesson);
        mGridData = new ArrayList<>();
        mGridAdapter = new LessonGridAdapter(this, R.layout.grid_item_lesson, mGridData);
        //Initialize with empty data     
        new AsyncTask<Void,Void,Void>() {
        	private ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
            	dialog = ProgressDialog.show(Lesson.this, "", "Loading...");
            }
            @Override
            protected Void doInBackground(Void... voids) {
            	parseResult(loadJSONFromAsset());
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                dialog.dismiss();
            }
        }.execute();
        mGridAdapter.setGridData(mGridData);
		mGridView.setAdapter(mGridAdapter);
		mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent myIntent = new Intent(Lesson.this, LessonActivity.class);
				Bundle extras = new Bundle();
				extras.putInt("position", position);
				extras.putString("title", mGridData.get(position).getTitle());
				extras.putString("image", mGridData.get(position).getImage());
				myIntent.putExtras(extras);
				startActivity(myIntent);
			}
		});
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("json/lesson/lesson.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray lesson = response.optJSONArray("lesson");
            LessonItem item;
            for (int i = 0; i < lesson.length(); i++) {
                JSONObject post = lesson.optJSONObject(i);
                String title = post.optString("title");
                String image = post.optString("image");
                item = new LessonItem();
                item.setTitle(title);
                item.setImage(image);
                mGridData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}