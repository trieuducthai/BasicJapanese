package tdt.basicjapanese.alphabet;

import java.util.ArrayList;
import tdt.basicjapanese.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AlphabetGridAdapter extends ArrayAdapter<AlphabetItem> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<AlphabetItem> mGridData = new ArrayList<AlphabetItem>();

    public AlphabetGridAdapter(Context mContext, int layoutResourceId, ArrayList<AlphabetItem> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }

    public void setGridData(ArrayList<AlphabetItem> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.tvText1 = (TextView) row.findViewById(R.id.tv_Text1);
            holder.tvText2 = (TextView) row.findViewById(R.id.tv_Text2);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        AlphabetItem item = mGridData.get(position);
        	holder.tvText1.setText(item.getKana());
	        holder.tvText2.setText(item.getRomaji());
        return row;
    }

    static class ViewHolder {
        TextView tvText1;
        TextView tvText2;
    }
}