package tdt.basicjapanese.alphabet;

import java.io.Serializable;


public class AlphabetItem implements Serializable  {
	
	private String kana;
	private String romaji;
	
	public AlphabetItem() {
		
	}
	
	public AlphabetItem(String kana, String romaji) {
		this.kana = kana;
		this.romaji = romaji;
	}
	
	public String getKana() {
		return kana;
	}
	
	public AlphabetItem setKana(String kana) {
		this.kana = kana;
		return this;			
	}
	
	public String getRomaji() {
		return romaji;
	}
	
	public AlphabetItem setRomaji(String romaji) {
		this.romaji = romaji;
		return this;			
	}
}
