package tdt.basicjapanese.alphabet;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import tdt.basicjapanese.R;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ImageView;

public class AlphabetActivity extends ActionBarActivity implements OnTouchListener {
	private ImageView imgChar;
	private ImageView imgWrite;
	private ImageButton btnBack;
	private ImageButton btnSpeaker;
	private ImageButton btnReload;
	private ImageButton btnNext;
	private Bitmap bitmap;
	private Canvas canvas;
	private Paint paint;
	private int pos, length;
	private String key, imageName, romaji, kana;
	private ArrayList<AlphabetItem> mGridData;
	private float downx = 0, downy = 0, upx = 0, upy = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.writing_layout);
		final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        imgChar = (ImageView) findViewById(R.id.img_char);
		imgWrite = (ImageView) findViewById(R.id.img_write);
        btnBack = (ImageButton) findViewById(R.id.btn_back);
		btnSpeaker = (ImageButton) findViewById(R.id.btn_speaker);
		btnReload = (ImageButton) findViewById(R.id.btn_reload);
		btnNext = (ImageButton) findViewById(R.id.btn_next);
    	Bundle extras = getIntent().getExtras();
        pos = extras.getInt("position");
        key = extras.getString("key");
    	mGridData = (ArrayList<AlphabetItem>) extras.getSerializable("list");
        length = mGridData.size();
        getAllImage();
		buttonClick();
	}
	
	private void getAllImage() {
		// TODO Auto-generated method stub
		romaji = mGridData.get(pos).getRomaji();
		kana = mGridData.get(pos).getKana();
		InputStream is = null;
		try {
				if (kana.equals("ぢ") || kana.equals("ヂ")) imageName = "ji2";
				else if (kana.equals("づ") || kana.equals("ヅ")) imageName = "zu2";
				else imageName = romaji;
		is = this.getResources().getAssets().open("image/" + key + "/" + imageName + ".PNG");
		} catch (IOException e) {
		  Log.w("EL", e);
		}

		Bitmap image = BitmapFactory.decodeStream(is);
		imgChar.setImageBitmap(image);
		imgWrite.setBackgroundResource(R.drawable.bg_write);
		Drawable d = getResources().getDrawable(R.drawable.bg_write);
		int dh = d.getIntrinsicHeight(); 
		int dw = d.getIntrinsicWidth();
		bitmap = Bitmap.createBitmap((int) dw, (int) dh, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bitmap);
		paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setColor(Color.DKGRAY);
		paint.setAntiAlias(true);
		paint.setDither(true);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setStrokeWidth(19);
		imgWrite.setImageBitmap(bitmap);
		imgWrite.setOnTouchListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    super.onCreateOptionsMenu(menu);
	    menu.add(Menu.NONE, 1, Menu.NONE, "Kiểm tra").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
	    return true;
	}

	@Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case 1:
            	Intent myIntent = new Intent(this, AlphabetTest.class);
				Bundle extras = new Bundle();
			    extras.putSerializable("list", mGridData);
				myIntent.putExtras(extras);
				startActivity(myIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			downx = event.getX();
			downy = event.getY();
			break;
		case MotionEvent.ACTION_MOVE:
			upx = event.getX();
			upy = event.getY();
			canvas.drawLine(downx, downy, upx, upy, paint);
			imgWrite.invalidate();
			downx = upx;
			downy = upy;
			break;
		case MotionEvent.ACTION_UP:
			upx = event.getX();
			upy = event.getY();
			canvas.drawLine(downx, downy, upx, upy, paint);
			imgWrite.invalidate();
			break;
		case MotionEvent.ACTION_CANCEL:
			break;
		default:
			break;
		}
		return true;
	}
	
	private void buttonClick() {
		
		btnBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 new Handler().postDelayed(new Runnable() {
		                @Override
		                public void run() {
		                	btnBack.setImageResource(R.drawable.back);
		                }
		            }, 100);
				 btnBack.setImageResource(R.drawable.back_use);
				 if (pos == 0) pos = length - 1;
				 else pos = pos - 1;
				 while (mGridData.get(pos).getRomaji().isEmpty()) {
					  if (mGridData.get(pos).getRomaji().isEmpty()) pos = pos - 1;
				 }
				 getAllImage(); 
			}
		});

		btnSpeaker.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final AudioManager mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
				final int originalVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
				mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
				MediaPlayer mediaPlayer = new MediaPlayer();
				mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
				try {
					AssetFileDescriptor descriptor = getAssets().openFd("audio/alphabet/" + mGridData.get(pos).getRomaji() + ".mp3");
					mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
					descriptor.close();
					mediaPlayer.prepare();
					mediaPlayer.start();
					btnSpeaker.setBackgroundResource(R.drawable.speaker_use);
				} catch (IOException e) {
					Log.v("AUDIO_LAYER", e.getMessage());
				}
				mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
		            public void onCompletion(MediaPlayer mp) {
		            	btnSpeaker.setBackgroundResource(R.drawable.speaker);
		            	mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, originalVolume, 0);
		            }
		        });
			}
		});

		btnReload.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				 new Handler().postDelayed(new Runnable() {
		                @Override
		                public void run() {
		                	btnReload.setImageResource(R.drawable.reload);
		                }
		            }, 100);
				 btnReload.setImageResource(R.drawable.reload_use);
				 getAllImage();
			}
		});

		btnNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				 new Handler().postDelayed(new Runnable() {
		                @Override
		                public void run() {
		                	btnNext.setImageResource(R.drawable.next);
		                }
		            }, 100);
				 btnNext.setImageResource(R.drawable.next_use);
				 if (pos == length - 1) pos = 0;
				 else pos = pos + 1;
				 while (mGridData.get(pos).getRomaji().isEmpty()) {
					  if (mGridData.get(pos).getRomaji().isEmpty()) pos = pos + 1;
				 }
				 getAllImage();
			}
		});
	}
}
