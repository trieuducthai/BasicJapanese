package tdt.basicjapanese.alphabet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import tdt.basicjapanese.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AlphabetTest extends ActionBarActivity {
	private TextView tvTest, tvTrue, tvFalse;
	private EditText edtAnswer;
	private Button btnHelp, btnAnswer;
	private ArrayList<AlphabetItem> mGridData;
	private ArrayList<Integer> number;
	private int length, pos;
	private int resultTrue;
	private int resultFalse;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alphabet_test);
		final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        tvTest = (TextView) findViewById(R.id.tv_test);
        tvTrue = (TextView) findViewById(R.id.tv_true);
        tvFalse = (TextView) findViewById(R.id.tv_false);
        edtAnswer = (EditText) findViewById(R.id.edt_answer);
        btnHelp = (Button) findViewById(R.id.btn_help);
        btnAnswer = (Button) findViewById(R.id.btn_answer);
        Bundle extras = getIntent().getExtras();
    	mGridData = (ArrayList<AlphabetItem>) extras.getSerializable("list");
        createListNumber();
        
        btnHelp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				edtAnswer.setHint(mGridData.get(Integer.parseInt(number.get(pos).toString())).getRomaji());
			}
		});
        btnAnswer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (pos < length-1) {
					String txtAnswer = edtAnswer.getText().toString();
					String txtResult = mGridData.get(Integer.parseInt(number.get(pos).toString())).getRomaji();
					if (txtAnswer.equals(txtResult)) {
						resultTrue++;
						tvTrue.setText(String.valueOf(resultTrue));
					}
					else {
						resultFalse++;
						tvFalse.setText(String.valueOf(resultFalse));
					}
					pos++;
					edtAnswer.setText("");
					edtAnswer.setHint("");
					tvTest.setText(mGridData.get(Integer.parseInt(number.get(pos).toString())).getKana());
				}
				else {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AlphabetTest.this);
					
					  alertDialogBuilder.setTitle("Kết thúc");
					
				      alertDialogBuilder.setMessage("Số câu trả lời đúng: "+resultTrue+"/"+(length-1));
				   
				      alertDialogBuilder.setPositiveButton("Kiểm tra lại", new DialogInterface.OnClickListener() {
				         @Override
				         public void onClick(DialogInterface arg0, int arg1) {
				        	 arg0.cancel();
				        	 createListNumber();
				         }
				      });
				      
				      alertDialogBuilder.setNegativeButton("Thoát",new DialogInterface.OnClickListener() {
				         @Override
				         public void onClick(DialogInterface dialog, int which) {
				            finish();
				         }
				      });
				      
				      AlertDialog alertDialog = alertDialogBuilder.create();
				      alertDialog.show();
				}
			}
		});
	}
	
	private void createListNumber() {
		number = new ArrayList<Integer>();
		for (int i = 0; i < mGridData.size(); i++) {
			if (!mGridData.get(i).getKana().isEmpty())
				number.add(i);
		}
		Collections.shuffle(number);
		length = number.size();
        pos = 0;
        tvTest.setText(mGridData.get(Integer.parseInt(number.get(pos).toString())).getKana());
        resultTrue = 0;
        tvTrue.setText(String.valueOf(resultTrue));
        resultFalse = 0;
        tvFalse.setText(String.valueOf(resultFalse));
	}

	@Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
