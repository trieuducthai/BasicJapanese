package tdt.basicjapanese.alphabet;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tdt.basicjapanese.R;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

public class KatakanaFragment extends Fragment {

	private static final String ARG_SECTION_NUMBER = "section_number";
    private GridView mGridView;
    private AlphabetGridAdapter mGridAdapter;
    private ArrayList<AlphabetItem> mGridData;

	public static KatakanaFragment newInstance(int sectionNumber) {
		KatakanaFragment fragment = new KatakanaFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public KatakanaFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final Context context = this.getContext();
		View rootView = inflater.inflate(R.layout.alphabet_fragment, container, false);
		mGridView = (GridView) rootView.findViewById(R.id.grid_alphabet);
        mGridData = new ArrayList<>();
        mGridAdapter = new AlphabetGridAdapter(context, R.layout.grid_item_alphabet, mGridData);
      //Initialize with empty data     
        new AsyncTask<Void,Void,Void>() {
        	private ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
            	dialog = ProgressDialog.show(context, "", "Loading...");
            }
            @Override
            protected Void doInBackground(Void... voids) {
            	parseResult(loadJSONFromAsset());
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                dialog.dismiss();
            }
        }.execute();
        mGridAdapter.setGridData(mGridData);
		mGridView.setAdapter(mGridAdapter);
		mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (!mGridData.get(position).getRomaji().isEmpty())
				{
					Intent myIntent = new Intent(getActivity(), AlphabetActivity.class);
					Bundle extras = new Bundle();
					extras.putInt("position", position);
					extras.putString("key", "katakana");
					extras.putSerializable("list", mGridData);
					myIntent.putExtras(extras);
					startActivity(myIntent);
				}
			}
		});
		return rootView;
	}
	
	public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getContext().getAssets().open("json/kana.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray kana = response.optJSONArray("kana");
            AlphabetItem item;
            for (int i = 0; i < kana.length(); i++) {
                JSONObject post = kana.optJSONObject(i);
                String katakana = post.optString("katakana");
                String romaji = post.optString("romaji");
                item = new AlphabetItem();
                item.setKana(katakana);
                item.setRomaji(romaji);
                mGridData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}