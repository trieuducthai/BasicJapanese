package tdt.basicjapanese.lesson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import tdt.basicjapanese.R;

public class LessonGridAdapter extends ArrayAdapter<LessonItem> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<LessonItem> mGridData = new ArrayList<LessonItem>();

    public LessonGridAdapter(Context mContext, int layoutResourceId, ArrayList<LessonItem> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }

    public void setGridData(ArrayList<LessonItem> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.titleTextView = (TextView) row.findViewById(R.id.tv_mainlesson);
            holder.imageView = (ImageView) row.findViewById(R.id.img_mainlesson);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        LessonItem item = mGridData.get(position);
        holder.titleTextView.setText(item.getTitle());
        InputStream is = null;
        try {
			is = mContext.getResources().getAssets().open("image/lesson/"+item.getImage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Bitmap image = BitmapFactory.decodeStream(is);
        holder.imageView.setImageBitmap(image);
        return row;
    }

    static class ViewHolder {
        TextView titleTextView;
        ImageView imageView;
    }
}