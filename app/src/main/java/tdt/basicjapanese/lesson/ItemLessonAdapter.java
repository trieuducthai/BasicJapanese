package tdt.basicjapanese.lesson;

import java.util.ArrayList;

import tdt.basicjapanese.R;

import android.app.Activity;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class ItemLessonAdapter extends ArrayAdapter<LessonListData> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<LessonListData> mListData = new ArrayList<LessonListData>();

    public ItemLessonAdapter(Context mContext, int layoutResourceId, ArrayList<LessonListData> mListData) {
        super(mContext, layoutResourceId, mListData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mListData = mListData;
    }

    public void setListData(ArrayList<LessonListData> mListData) {
        this.mListData = mListData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.tvName = (TextView) row.findViewById(R.id.tv_namePerson);
            holder.tvJP = (TextView) row.findViewById(R.id.tv_jpLI);
            holder.tvVN = (TextView) row.findViewById(R.id.tv_vnLI);
            //holder.ibAudio = (ImageButton) row.findViewById(R.id.ib_audioLI);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        LessonListData item = mListData.get(position);
        holder.tvName.setText(item.getName());
        holder.tvJP.setText(item.getJP());
        holder.tvVN.setText(item.getVN());
        return row;
    }

    static class ViewHolder {
        TextView tvName;
        TextView tvJP;
        TextView tvVN;
        //ImageButton ibAudio;
    }
}