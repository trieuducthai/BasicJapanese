package tdt.basicjapanese.lesson;

public class LessonListData {
	private String name;
	private String jp;
	private String vn;
	private String audio;
	
	public LessonListData() {
		
	}
	
	public LessonListData(String name, String jp, String vn, String audio) {
		this.name = name;
		this.jp = jp;
		this.vn = vn;
		this.audio = audio;
	}
	
	public String getName() {
		return name;
	}
	
	public LessonListData setName(String name) {
		this.name = name;
		return this;			
	}
	
	public String getJP() {
		return jp;
	}
	
	public LessonListData setJP(String jp) {
		this.jp = jp;
		return this;				
	}
	
	public String getVN() {
		return vn;
	}
	
	public LessonListData setVN(String vn) {
		this.vn = vn;
		return this;				
	}
	
	public String getAudio() {
		return audio;
	}
	
	public LessonListData setAudio(String audio) {
		this.audio = audio;
		return this;				
	}
}
