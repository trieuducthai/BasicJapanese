package tdt.basicjapanese.lesson;

public class LessonItem {
	private String title;
	private String image;
	
	public LessonItem() {
		
	}
	
	public LessonItem(String title, String image) {
		this.title = title;
		this.image = image;
	}
	
	public String getTitle() {
		return title;
	}
	
	public LessonItem setTitle(String title) {
		this.title = title;
		return this;			
	}
	
	public String getImage() {
		return image;
	}
	
	public LessonItem setImage(String image) {
		this.image = image;
		return this;				
	}
}
