package tdt.basicjapanese.lesson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import tdt.basicjapanese.R;

public class LessonActivity extends ActionBarActivity {

	private int pos;
	private String title;
	private String image;
	private TextView tvTitle, tvDetail, tvGrammar;
	private ImageButton btnMainAudio;
	private ImageView imgLesson;
	//private ListView list;
	private ItemLessonAdapter mListAdapter;
    private ArrayList<LessonListData> mListData;
	private MediaPlayer mp;
	private LinearLayout listViewReplacement;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.item_lesson);
		final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        tvTitle = (TextView) findViewById(R.id.tv_lessontitle);
        imgLesson = (ImageView) findViewById(R.id.img_lesson);
        tvDetail = (TextView) findViewById(R.id.tv_lessondetail);
        tvGrammar = (TextView) findViewById(R.id.tv_grammar);
        btnMainAudio = (ImageButton) findViewById(R.id.btn_mainaudio);
        
        listViewReplacement = (LinearLayout) findViewById(R.id.list_lesson);
        mListData = new ArrayList<>();
        mListAdapter = new ItemLessonAdapter(this, R.layout.list_item_lesson, mListData);
        
        Bundle extras = getIntent().getExtras();
        pos = extras.getInt("position");
        title = extras.getString("title");
        tvTitle.setText(title);
        image = extras.getString("image");
        InputStream is = null;
        try {
			is = getResources().getAssets().open("image/lesson/"+image);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Bitmap image = BitmapFactory.decodeStream(is);
        imgLesson.setImageBitmap(image);
        parseResult(loadJSONFromAsset(pos));
        mListAdapter.setListData(mListData);
        for (int i = 0; i < mListAdapter.getCount(); i++) {
            View view = mListAdapter.getView(i, null, listViewReplacement);
            ImageButton ibAudio = (ImageButton) view.findViewById(R.id.ib_audioLI);
            setOnClick(ibAudio, mListData.get(i).getAudio());
            listViewReplacement.addView(view);
        }
	}
	
	private void setOnClick(final ImageButton btn, final String str) {
		btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	try {
            		if (mp != null) {
                        mp.stop();
                        mp.release();
                        mp = null;
            		}
                		mp = new MediaPlayer();
                		AssetFileDescriptor descriptor = getAssets().openFd("audio/lesson/" + str);
            			mp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            			descriptor.close();
            			mp.prepare();
            			mp.start();
            			btn.setBackgroundResource(R.drawable.speaker_use);
            			mp.setOnCompletionListener(new OnCompletionListener() {
            	            public void onCompletion(MediaPlayer mp) {
            	            	btn.setBackgroundResource(R.drawable.speaker);
            	            }
            	        });
        		} catch (IOException e) {
        			Log.v("AUDIO_LAYER", e.getMessage());
        		}
            }
	    });
	}
	
	@Override
	public void onBackPressed() {
		if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
		}
	    super.onBackPressed();
	}
	
	@Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	 public static void setListViewHeightBasedOnChildren(ListView listView) {
	        ListAdapter listAdapter = listView.getAdapter();
	        if (listAdapter == null) {
	            // pre-condition
	        return;
	    }
	
	    int totalHeight = 0;
	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.EXACTLY);
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        View listItem = listAdapter.getView(i, null, listView);
	        listItem.measure(MeasureSpec.makeMeasureSpec(desiredWidth, MeasureSpec.AT_MOST),
	                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
	        totalHeight += listItem.getMeasuredHeight();
	        System.out.println(listItem.getMeasuredHeight() + " --- " + totalHeight);
	    }
	
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight
	            + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
	
	public String loadJSONFromAsset(int position) {
        String json = null;
        try {
        	String url = "json/lesson/lesson_"+String.valueOf(position+1)+".json";
            InputStream is = getAssets().open(url);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray lesson = response.optJSONArray("lesson_"+String.valueOf(pos+1));
            for (int i = 0; i < lesson.length(); i++) {
                JSONObject post = lesson.optJSONObject(i);
                String detail = post.optString("detail");
                tvDetail.setText(detail);
                String mainAudio = post.optString("main_audio");
                setOnClick(btnMainAudio, mainAudio);
                String grammar = post.optString("grammar");
                tvGrammar.setText(grammar);
                
                final ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
                SimpleAdapter mSchedule = null;
                HashMap<String, String> map;

                LessonListData item;
                
                JSONArray dialogue = post.optJSONArray("dialogue");
                for (int j = 0; j < dialogue.length(); j++) {
                    JSONObject post2 = dialogue.optJSONObject(j);
                    item = new LessonListData();
                    String kanaName = post2.optString("kana_name");  
                    String romajiName = post2.optString("romaji_name");
                    item.setName(kanaName + "\n" + romajiName);
                    String kana = post2.optString("kana");
                    String romaji = post2.optString("romaji");
                    item.setJP(kana + "\n" + romaji);
                    String vn = post2.optString("vn");
                    item.setVN(vn);
                    String audio = post2.optString("audio");
                    item.setAudio(audio);
                    mListData.add(item);
                } 
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}